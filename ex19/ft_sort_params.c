#include	<unistd.h>

void	ft_putstr(char *str)
{
	while (*str)
	{
		write(1, &*str, 1);
		str++;
	}
}

int		ft_strcmp(char *s1, char *s2)
{
	while (*s1 && *s2)
	{
		if (*s1 != *s2)
			return (*s1 - *s2);
		s1++;
		s2++;
	}
	return ((unsigned char)*s1 - (unsigned char)*s2);
}

void	ft_sort_params(int ac, char **av)
{
	char	*item;
	int		i;
	int		j;

	i	=	1;
	while (i < ac)
	{
		j	=	1;
		while (j < (ac - 1))
		{
			if (ft_strcmp(av[j], av[j + 1]) > 0)
			{
				item		= av[j];
				av[j]		= av[j + 1];
				av[j + 1]	= item;
			}
			j++;
		}
		i++;
	}
}

int		main(int ac, char **av)
{
	int		n;

	n = 1;
	if (ac > 1)
	{
		ft_sort_params(ac, av);
		while (n < ac)
		{
			ft_putstr(av[n]);
			ft_putstr("\n");
			n++;
		}
	}
	else
		ft_putstr("\n");
	return (0);
}
