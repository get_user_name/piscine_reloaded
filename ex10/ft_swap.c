void	ft_swap(int *a, int *b)
{
	int		item;

	item	= *a;
	*a		= *b;
	*b		= item;
}
