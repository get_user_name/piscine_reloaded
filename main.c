#include	<stdio.h>
#include	<string.h>
#include	<inttypes.h>

#include	"ex06/ft_print_alphabet.c"
#include	"ex07/ft_print_numbers.c"
#include	"ex08/ft_is_negative.c"
#include	"ex09/ft_ft.c"
#include	"ex10/ft_swap.c"
#include	"ex11/ft_div_mod.c"
#include	"ex12/ft_iterative_factorial.c"
#include	"ex13/ft_recursive_factorial.c"
#include	"ex14/ft_sqrt.c"
#include	"ex15/ft_putstr.c"
#include	"ex16/ft_strlen.c"
#include	"ex17/ft_strcmp.c"
#include	"ex20/ft_strdup.c"
#include	"ex21/ft_range.c"
#include	"ex22/ft_abs.h"
#include	"ex23/ft_point.h"
#include	"ex25/ft_foreach.c"
#include	"ex26/ft_count_if.c"

// ex23
//#############################
void set_point(t_point *point)
{
	point->x = 42;
	point->y = 21;
}
//##############################

// ex25
//#############################
void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putnbr(int nb)
{
	if (nb < 0)
	{
		ft_putchar('-');
		if(nb == -2147483648)
		{
			ft_putchar('2');
			nb = -147483648 ;
		}
		nb *= -1;
	}
	if (nb != 0 && ((nb / 10) != 0))
		ft_putnbr(nb / 10);
	ft_putchar(nb % 10 + '0');
}
//##############################

// ex26
//#############################
int		ft_test(char *str)
{
	int		n;
	
	n = -1;
	while (str[++n])
		if (str[n] == 'k')
			return (1);
	return (0);
}

int		ft_test2(char *str)
{
	int		n;
	
	n = -1;
	while (str[++n])
		if (str[n] == 's')
			return (1);
	return (0);
}
//##############################

int		main(void)
{
	// // ex06
	// //##############################
	// ft_print_alphabet();
	// //##############################

	// // ex07
	// //##############################
	// ft_print_numbers();
	// //##############################

	// // ex08
	// //##############################
	// ft_is_negative(-2147483648);
	// ft_is_negative(-1);
	// ft_is_negative(0);
	// ft_is_negative(1);
	// ft_is_negative(2147483647);
	// //##############################

	// // ex09
	// //##############################
	// int	test;

	// test = 1;
	// printf("%d", test);
	// ft_ft(&test);
	// printf(" - %d\n", test);
	// test = -10;
	// printf("%d", test);
	// ft_ft(&test);
	// printf(" - %d\n", test);
	// test = -2147483648;
	// printf("%d", test);
	// ft_ft(&test);
	// printf(" - %d\n", test);
	// //##############################

	// // ex10
	// //##############################
	// int	a;
	// int	b;

	// a = 10;
	// b = 20;
	// printf("%d - %d/", a, b);
	// ft_swap(&a, &b);
	// printf("%d - %d\n", a, b);
	// ft_swap(&a, &b);
	// printf("%d - %d/", a, b);
	// ft_swap(&a, &b);
	// printf("%d - %d\n", a, b);
	// //##############################

	// // ex11
	// //##############################
	// int	a;
	// int	b;
	// int div;
	// int	mod;

	// a = 10;
	// b = 3;
	// ft_div_mod(a, b, &div, &mod);
	// printf("%d - %d\n", div, mod);

	// a = 10;
	// b = 2;
	// ft_div_mod(a, b, &div, &mod);
	// printf("%d - %d\n", div, mod);
	// //##############################

	// // ex12
	// //##############################
	// int	f;

	// f = 5;
	// f = ft_iterative_factorial(f);
	// printf("%d\n", f);

	// f = -1;
	// f = ft_iterative_factorial(f);
	// printf("%d\n", f);

	// f = 0;
	// f = ft_iterative_factorial(f);
	// printf("%d\n", f);

	// f = 1;
	// f = ft_iterative_factorial(f);
	// printf("%d\n", f);

	// f = 8;
	// f = ft_iterative_factorial(f);
	// printf("%d\n", f);
	// //##############################

	// // ex13
	// //##############################
	// int	f;

	// f = 5;
	// f = ft_recursive_factorial(f);
	// printf("%d\n", f);

	// f = -1;
	// f = ft_recursive_factorial(f);
	// printf("%d\n", f);

	// f = 0;
	// f = ft_recursive_factorial(f);
	// printf("%d\n", f);

	// f = 1;
	// f = ft_recursive_factorial(f);
	// printf("%d\n", f);

	// f = 8;
	// f = ft_recursive_factorial(f);
	// printf("%d\n", f);
	// //##############################

	// // ex14
	// //##############################
	// printf("%d==2\n", ft_sqrt(4));
	// printf("%d==3\n", ft_sqrt(9));
	// printf("%d==4\n", ft_sqrt(16));
	// printf("%d==11\n", ft_sqrt(121));
	// printf("%d==0\n", ft_sqrt(7));
	// printf("%d==0\n", ft_sqrt(329));
	// //##############################

	// // ex15
	// //##############################
	// ft_putstr("Hello!\n");
	// ft_putstr("qwertyuiop[]asdfghjkl;'zxcvbnm,./1234567890-=/*-+\n");
	// //##############################

	// // ex16
	// //##############################
	// printf("%d=7\n", ft_strlen("Hello!\n"));
	// printf("%d=50\n", ft_strlen("qwertyuiop[]asdfghjkl;'zxcvbnm,./1234567890-=/*-+\n"));
	// //##############################

	// // ex17
	// //##############################
	// printf("%d=%d\n", ft_strcmp("test", "test"), strcmp("test", "test"));
	// printf("%d=%d\n", ft_strcmp("te", "test"), strcmp("te", "test"));
	// printf("%d=%d\n", ft_strcmp("aab", "aaa"), strcmp("aab", "aaa"));
	// //##############################

	// // ex20
	// //##############################
	// char str1[] = "hello!";
	// char *copy;

	// copy = ft_strdup(str1);
	// printf("%s=%s\n", copy, str1);

	// //тупые тесты
	// char* str;
	// char* res;
	// str = "asdf", res = ft_strdup(str);
	// printf("\n Тупые тесты\n1, 'asdf' vs 'asdf'\n1, '' vs ''\n1, 'hello world!' vs 'hello world!\n----------------------------------\n");
	// printf("%d, '%s' vs '%s'\n", res != str, res, str);
	// str = "", res = ft_strdup(str);
	// printf("%d, '%s' vs '%s'\n", res != str, res, str);
	// str = "hello world!", res = ft_strdup(str);
	// printf("%d, '%s' vs '%s'\n", res != str, res, str);
	// //##############################

	// // ex21
	// //##############################
	// int		*nbr;

	// nbr	= ft_range(3, 5);
	// for (int n = 0; n < 2; n++)
	// 	printf("%d ", nbr[n]);
	// printf("\n");

	// //тупые тесты
	// // char* str;
	// printf("\nТупые тесты\n5,6,7,8,9,\n-20,-19,-18,-17,-16,\n100,\n0\n----------------------------------\n");
	// int* res;
	// int i;
	// res = ft_range(5, 10);
	// for (i = 0; i < 5; i++)
	// 	printf("%d,", res[i]);
	// printf("\n");

	// res = ft_range(-20, -15);
	// for (i = 0; i < 5; i++)
	// 	printf("%d,", res[i]);
	// printf("\n");

	// res = ft_range(100, 101);
	// for (i = 0; i < 1; i++)
	// 	printf("%d,", res[i]);
	// printf("\n");

	// res = ft_range(10, 5);
	// printf("%" PRIxPTR "\n", (uintptr_t) res);
	// //##############################

	// // ex22
	// //##############################
	// int	n = -2147483648;

	// n = 32;
	// printf("%d\n", ABS(n));
	// n = 32;
	// printf("%d\n", ABS(n));
	// n = 2147483647;
	// printf("%d\n", ABS(n));
	// n = -2147483648;
	// printf("%d\n", ABS(n));
	// //##############################

	// // ex23
	// //##############################
	// t_point		point;

	// set_point(&point);
	// printf("%d/%d==42/21\n", (int)point.x, (int)point.y);
	// //##############################

	// // ex25
	// //##############################
	// int		*test;
	// int		n;
	
	// test	= (int*)malloc(sizeof(int) * 1336);
	// for(n = 0; n < 1337; n++)
	// 	test[n] = n;
	// ft_foreach(test, 1337, &ft_putnbr);


	// int test1[] = {1,2,3,4,5};
	// int test2[] = {-25,0,20,45};
	// int test3[] = {5};
	
	// printf("\n\nТупые тесты\n12345\n-25020\n\n----------------------------------\n");
	// ft_foreach(test1, 5, ft_putnbr);
	// printf("\n");
	// ft_foreach(test2, 3, ft_putnbr);
	// printf("\n");
	// ft_foreach(test3, 0, ft_putnbr);
	// printf("\n");
	// //##############################

	// // ex25
	// //##############################	
	// char	*str[4];
	// str[0] = (char*)malloc(sizeof(char)*3);
	// str[0] = "ok\0"; 
	// str[1] = (char*)malloc(sizeof(char)*4);
	// str[1] = "kok\0"; 
	// str[2] = (char*)malloc(sizeof(char)*2);
	// str[2] = "t\0"; 
	// str[3] = NULL;
	// printf("%d=2\n", ft_count_if(str, ft_test));
	// printf("%d=0\n", ft_count_if(str, ft_test2));
	// //##############################

	return (0);
}