#include	"../includes/ft.h"
#include	<stdio.h>

void	ft_putstr(const char *str)
{
	int		n;
	
	n = -1;
	while (str[++n])
		write(2, &str[n], 1);
}

void	ft_cat(char *str)
{
	int		file;
	char	buffer;

	file = open(str, O_RDWR);
	while (read(file, &buffer, 1))
		write(1, &buffer, 1);
	close(file);
}

int		main(int ac, char **av)
{
	if (ac == 2)
		ft_cat(av[1]);
	else if (ac == 1)
		ft_putstr("File name missing.\n");
	else if (ac > 2)
		ft_putstr("Too many arguments.\n");\
	return (0);
}

