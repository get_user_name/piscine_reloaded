#include	<stdint.h>

void	ft_foreach(int *tab, int length, void(*f)(int))
{
	int		n;

	if (length < 1)
		return ;
	n = 0;
	while (n < length)
	{
		f(tab[n]);
		n++;
	}
}
