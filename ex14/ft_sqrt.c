int		ft_sqrt(int nb)
{
	int		n;

	n = 1;
	while (n < (nb / 2 + 1))
		if (n * n == nb)
			return (n);
		else
			n++;
	return (0);
}
