#include	<stdlib.h>

int		*ft_range(int min, int max)
{
	int		*answer;
	int		n;

	if (min >= max)
		return (0);
	answer = (int*)malloc(sizeof(int) * (max - min));
	n = 0;
	while (min < max)
	{
		answer[n] = min;
		n++;
		min++;
	}
	return (answer);
}
