int		ft_iterative_factorial(int nb)
{
	int		n;

	n = 1;
	if ((nb == 0) || (nb == 1))
		return (1);
	else if (nb > 1)
	{
		while (1 < nb)
		{
			n *= nb;
			nb--;
		}
		return (n);
	}
	return (0);
}
