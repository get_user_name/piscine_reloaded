#include	<unistd.h>

void	ft_putstr(char *str)
{
	while (*str)
	{
		write(1, &*str, 1);
		str++;
	}
}

int		main(int ac, char **av)
{
	int		n;

	n = 1;
	if (ac > 1)
		while (n < ac)
		{
			ft_putstr(av[n]);
			ft_putstr("\n");
			n++;
		}
	else
			ft_putstr("\n");		
	return (0);
}
