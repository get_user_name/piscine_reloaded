echo "##ex18## standart output"
echo "##########################################################"
echo "- test1 test2 test3 -"
gcc -Wall -Wextra -Werror ex18/ft_print_params.c -o a.out && ./a.out test1 test2 test3
echo "----------------------------------------------------------"
echo "-   -"
gcc -Wall -Wextra -Werror ex18/ft_print_params.c -o a.out && ./a.out
echo "##########################################################"
echo "##ex19## sort and standart output"
echo "##########################################################"
echo "- test5 test1 test3 test2 -"
gcc -Wall -Wextra -Werror ex19/ft_sort_params.c -o a.out && ./a.out test5 test1 test3 test2
echo "----------------------------------------------------------"
echo "-   -"
gcc -Wall -Wextra -Werror ex19/ft_sort_params.c -o a.out && ./a.out
echo "##########################################################"
echo "##ex27## small ft_cat"
echo "##########################################################"
cd ex27
make
./ft_display_file
echo "File name missing"
echo "---------------------------------"
./ft_display_file Makefile
echo "---------------------------------"
./ft_display_file Makefile display_file.c
echo "Too many arguments."
make clean
make re
make fclean
echo "##########################################################"
