#ifndef		FT_ABS_H
# define	FT_ABS_H

# define ABS(Value) (Value < 0 ? MODULE(Value): Value)
# define MODULE(Value) (Value == -2147483648 ? 2147483647 : -Value)

#endif