#include <stdlib.h> 
#include <stdio.h>

int		ft_strlen1(char *str)
{
	int		n;

	n = 0;
	while (*str++)
		n++;
	return (n);
}

char	*ft_strdup(char *src)
{
	char	*copy;
	int		n;

	if (!src[0])
		return ("");
	copy = (char*)malloc(sizeof(char) * (ft_strlen1(src) + 1));
	if (!copy)
		return (NULL);
	n = 0;
	while (src[n])
	{
		copy[n] = src[n];
		n++;
	}
	copy[n] = '\0';
	return (copy);
}
